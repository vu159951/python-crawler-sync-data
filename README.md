# Chord Finder Python Syncing Data

Find any chords of various songs over internet.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
- Python 2.7
- MongoDB

```

### Installing

Step 1: Edit settings.py file
 - Save mongoDB connection config

Step 2: Install python requirement
  - Run `pip install -r requirements.txt `

Step 3: Start app.py
 - Windows: python app.py


## Authors

* **Vu Tran** - *Initial work* - [vu159951](https://gitlab.com/vu159951/python-crawler-sync-data)

See also the list of [contributors](https://gitlab.com/vu159951/python-crawler-sync-data/graphs/master) who participated in this project.