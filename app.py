from flask import Flask
app = Flask(__name__)

import subprocess
# import pymongo
import redis

# client = pymongo.MongoClient('mongodb://localhost:27017/')
# db_connection = client['dev']
# collection = db_connection['mp3files']

@app.route('/syncData')
def hello_world():
    # set redis status
    r = redis.StrictRedis(host='localhost', port=6379, db=0)

    r.set('job', 'Started!')

    # call spiders
    spider_name = "chordify_"
    subprocess.check_output(['scrapy', 'crawl', spider_name])
    r.set('job', 'Done!')

    return r.get('job')

if __name__ == '__main__':
    app.run(debug=True)

