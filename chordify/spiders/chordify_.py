# -*- coding: utf-8 -*-
import scrapy
import pymongo
import pydash
import json

client = pymongo.MongoClient('mongodb://localhost:27017/')
db_connection = client['dev']
collection = db_connection['mp3files']


class ChordifySpider(scrapy.Spider):
    name = 'chordify_'
    allowed_domains = ['chordify.net']
    cursor = collection.find({'migrated': False})
    unprocessed_ids = pydash.map_(cursor, 'hash')

    start_urls = []
    for id in unprocessed_ids:
        start_urls.append('https://chordify.net/song/data/file:' + id +'.mp3?vocabulary=extended_inversions')


    def parse(self, response):
        jsonresponse = json.loads(response.body_as_unicode())
        yield jsonresponse